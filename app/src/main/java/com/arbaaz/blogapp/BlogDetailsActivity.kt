package com.arbaaz.blogapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.arbaaz.blogapp.http.Blog
import com.arbaaz.blogapp.http.BlogHttpClient
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_blog_details.*

class BlogDetailsActivity: AppCompatActivity() {
    companion object {
        private const val EXTRAS_BLOG = "EXTRAS_BLOG"

        fun start(activity: Activity, blog: Blog) {
            val intent = Intent(activity, BlogDetailsActivity::class.java)
            intent.putExtra(EXTRAS_BLOG, blog)
            activity.startActivity(intent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blog_details)

        imageBack.setOnClickListener { finish() }

        intent.extras?.getParcelable<Blog>(EXTRAS_BLOG)?.let{ blog ->
            showData(blog)
        }

    }

    private  fun loadData() {
        BlogHttpClient.loadBlogArticles(
            onSuccess = { list ->
                runOnUiThread{ showData(list[0]) }
            },
            onError = {
                showErrorSnackbar()
            }
        )
    }

    private fun showData(blog: Blog) {

        progressBar.visibility = View.GONE

        textTitle.text = blog.title
        textDate.text = blog.date
        textAuthor.text = blog.author.name
        textRating.text = blog.rating.toString()
        textViews.text = String.format("(%d Views)", blog.views)
        textDescription.text = Html.fromHtml(blog.description)
        ratingBar.rating = blog.rating

        /**
         * Image libraries for media management
         * 1. glide
         * 2. fresco
         * 3. picasso
         */

        Glide.with(this)
            .load(blog.getImageUrl())
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(imageMain)

        Glide.with(this)
            .load(blog.author.getAvatarUrl())
            .transform(CircleCrop())
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(imageAvatar)

    }

    private fun showErrorSnackbar() {
        val rootView = textAuthor.rootView
        Snackbar.make(snackbarPos,
            "Error while loading article!",
            Snackbar.LENGTH_INDEFINITE).run {
                setActionTextColor(resources.getColor(R.color.orange_500))
                setAction("Retry") {
                        loadData()
                        dismiss()
                }
            }.show()
    }
}
package com.arbaaz.blogapp.http

import android.util.Log
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.util.concurrent.Executors

object BlogHttpClient {
    public const val BASE_URL = "https://studylinkprojects.com/android-resources/blog/"
    private const val BLOG_ARTICLES_URL = BASE_URL + "/all-data.json"

    private val executor = Executors.newFixedThreadPool(4)
    private val client = OkHttpClient()
    private val gson = Gson()

    fun loadBlogArticles(onSuccess: (List<Blog>) -> Unit, onError: () -> Unit) {
        // 1. Make a request object
        val request = Request.Builder()
                            .get()
                            .url(BLOG_ARTICLES_URL)
                            .build()

        // 2. run an executor thread
        executor.execute {
            kotlin.runCatching {
                val response: Response = client.newCall(request).execute()
                response.body?.string()?.let { json ->
                    gson.fromJson(json, BlogData::class.java)?.let { blogData ->
                        return@runCatching blogData.data
                    }
                }
            }.onFailure { e: Throwable ->
                Log.e("BlogHttpClient", "Error loading blog articles", e)
                onError()

            }.onSuccess { value: List<Blog>? ->
                onSuccess(value ?: emptyList())
            }
        }

    }
}
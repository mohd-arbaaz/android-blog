package com.arbaaz.blogapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.arbaaz.blogapp.adapter.MainAdapter
import com.arbaaz.blogapp.databinding.ActivityMainBinding
import com.arbaaz.blogapp.http.Blog
import com.arbaaz.blogapp.http.BlogHttpClient
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_blog_details.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myAdapter = MainAdapter { blog ->
        BlogDetailsActivity.start(this, blog)

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        startActivity(Intent(this, BlogDetailsActivity::class.java))
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = myAdapter

        binding.refresh.setOnRefreshListener {
            loadData()
        }
        loadData()
    }

    private fun loadData() {
        binding.refresh.isRefreshing = true
        BlogHttpClient.loadBlogArticles(
            onSuccess = { blogList: List<Blog> ->
                runOnUiThread {
                    binding.refresh.isRefreshing = false
                    myAdapter.submitList(blogList)
                }

            },
            onError = {
                binding.refresh.isRefreshing = false
                runOnUiThread { showErrorSnackbar() }
            }
        )
    }

    private fun showErrorSnackbar() {
        val rootView = Snackbar.make(snackbarPos,
            "Error while loading article!",
            Snackbar.LENGTH_INDEFINITE).run {
            setActionTextColor(resources.getColor(R.color.orange_500))
            setAction("Retry") {
                loadData()
                dismiss()
            }
        }.show()
    }
}
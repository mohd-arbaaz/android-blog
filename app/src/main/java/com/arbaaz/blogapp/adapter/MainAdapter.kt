package com.arbaaz.blogapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.arbaaz.blogapp.databinding.ItemMainBinding
import com.arbaaz.blogapp.http.Blog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

/*
 * General Working of Recycler View
 * 1. It executes "onCreateViewHolder" the number of times which is equal to the number of list items that can appear on the screen at the same time!
 * 2. It executes "onBindViewHolder" every time we need to render a list item
 * 3. When a user scrolls the list, views that are not visible anymore gets reused with a corresponding view holder.
 */

/* RecyclerView is an API
It has 5 classes

RecyclerView.Adapter (When data arrives, it is huge, i will have to separate every object. The dataset is wrapped and views are created for individual items):-
It is an abstract class. Its abstract methods are:-
1. ViewHolder onCreateViewHolder(---)
2. void onBindViewHolder(ViewHolder obj, int position)
3. int getItemCount()

RecyclerView.ViewHolder (Holds all sub views that depends on the current items data. It is responsivle for changing data):-
It inherits from root ViewHolder and you can get access to the root view using a public member.

[Above 2 classes are inner classes of RecyclerView]
__________________________
LayoutManager (Places item within available area.)
(will be used automatically)
____________
(not imp)
ItemDecoration (Draws decorations around or on top of each items' view. eg: adding lines around each object)

ItemAnimator (Animates items when they are added, removed or reordered.)
*/

class MainAdapter(private val onItemClickListener: (Blog) -> Unit): ListAdapter<Blog, MainViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        // This method is called only a certain amount of times when the recycler view needs a new ViewHolder object.
        val inflator = LayoutInflater.from(parent.context)
        val binding = ItemMainBinding.inflate(inflator, parent, false)

        return MainViewHolder(binding, onItemClickListener)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        // This method is called every time when we need to render a list item (bind the model with ViewHolder)
        holder.bindTo(getItem(position))
    }
}

class MainViewHolder(private val binding: ItemMainBinding, private val onItemClickListener: (Blog) -> Unit): RecyclerView.ViewHolder(binding.root) {

    fun bindTo(blog: Blog) {
        binding.root.setOnClickListener { onItemClickListener(blog) }
        binding.textTitle.text = blog.title
        binding.textDate.text = blog.date

        Glide.with(itemView)
            .load(blog.author.getAvatarUrl())
            .transform(CircleCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.imageAvatar)
    }

}

/*
 * If we want our ListAdapter to work correctly, then we need to implement DiffUtil.ItemCallBack
 * This is a special callback that is used by the adapter to figure out if the items are the same!
 * If the item content is the same for the further difference calculation, it will re-render only items that have changed!
 *
 * DiffUtil.ItemCallback has two methods:
 * 1. areItemsTheSame(): where we compare two objects of type T
 * 2. areContentsTheSame(): where we compare two objects of type T
 */

private val DIFF_CALLBACK: DiffUtil.ItemCallback<Blog> = object: DiffUtil.ItemCallback<Blog>() {
    override fun areItemsTheSame(oldItem: Blog, newItem: Blog): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Blog, newItem: Blog): Boolean {
        return oldItem == newItem
    }
}